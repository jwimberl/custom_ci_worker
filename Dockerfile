# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

# install custom packages (in this example, ghostscript and ghostscript-devel)
### TODO: set the list of packages as necessary
RUN yum install -y ghostscript ghostscript-devel doxygen graphviz graphviz-devel texlive-epstopdf-bin && yum clean all
RUN which doxygen
RUN which dot
RUN which epstopdf
